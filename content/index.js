"use strict"

const {ipcRenderer} = require("electron")

const buttonAccept = document.getElementById("button-accept")
const buttonCancel = document.getElementById("button-cancel")
const buttonSkip = document.getElementById("button-skip")
const buttons = [buttonAccept, buttonCancel, buttonSkip]
const checkAutoDownload = document.getElementById("check-auto-download")
const progressContainer = document.querySelector("section.progress")
const progressBar = document.getElementById("progress-bar")
const icon = document.getElementById("icon")


buttonAccept && buttonAccept.addEventListener("click", e => {
    ipcRenderer.send("dblsqd-accept")
})
buttonCancel && buttonCancel.addEventListener("click", e => {
    ipcRenderer.send("dblsqd-cancel")
})
buttonSkip && buttonSkip.addEventListener("click", e => {
    ipcRenderer.send("dblsqd-skip")
})
checkAutoDownload && checkAutoDownload.addEventListener("change", e => {
    ipcRenderer.send("dblsqd-toggle-auto-download", e.target.checked)
})

const replaceInfo = function(string, info) {
    return string
        .replace("%APPNAME%", info.appName)
        .replace("%CURRENT_VERSION%", info.currentVersion)
        .replace("%UPDATE_VERSION%", info.updateVersion)
}

ipcRenderer.on("data-available", (sender, data) => {
    document.body.classList.add(data.os)
    document.title = replaceInfo(document.title, data)
    const headline = document.getElementById("headline")
    if (headline) headline.textContent = replaceInfo(headline.textContent, data)
    const info = document.getElementById("info")
    if (info) info.textContent = replaceInfo(info.textContent, data)
    const changelog = document.getElementById("changelog")
    if (changelog) {
        changelog.innerHTML = ""
        data.releases.forEach(release => {
            let h2 = document.createElement("h2")
            h2.innerText = release.version
            changelog.appendChild(h2)
            release.changelog
                .split("\n\n")
                .filter(paragraph => paragraph.trim() !== "")
                .forEach(paragraph => {
                    let p = document.createElement("p")
                    p.innerText = release.changelog
                    changelog.appendChild(p)
                })
        })
    }
    if (checkAutoDownload) checkAutoDownload.checked = data.autoDownload
    if (progressBar && data.downloadFinished) {
        progressBar.setAttribute("value", 1)
        progressContainer.style.display = "flex"
    }
    if (data.icon) {
        icon.style.backgroundImage = "url(" + data.icon + ")"
    } else {
        icon.style.display = "none"
    }
})

ipcRenderer.on("download-progress", (sender, bytesReceived, bytesTotal) => {
    let progress = Math.ceil(bytesReceived/bytesTotal*100)/100
    progressBar.setAttribute("value", progress)
    progressContainer.style.display = "flex"
})

ipcRenderer.on("disable-buttons", (sender, disable) => {
    disable = disable === false ? false : true
    buttons.forEach(button => {
        if (disable) {
            button.setAttribute("disabled", true)
        } else {
            button.removeAttribute("disabled")
        }
    })
})

ipcRenderer.send("dblsqd-ready")
