This module provides a drop-in auto-update solution for Electron applications.

## Installation

You can install *dblsqd-electron* via npm. Add *dblsqd-electron* to the
`package.json` of your your Electron app or install it via the command-line:
`npm i --save dblsqd-sdk`

## Usage

You can add a complete auto-update UI to your Electron app with two lines of
code (or four lines if you insist on counting the `require` statements):

```javascript
const {Feed} = require("dblsqd-sdk")
const {UpdateWindow} = require("dblsqd-electron")

let feed = new Feed("https://feeds.dblsqd.com/:app_token", ":channel", ":os", ":arch")
let updateWindow = new UpdateWindow(feed)
```

## Configuration

### Setting an Icon
`UpdateWindow` can display an icon when the `icon` option is set:
```javascript
new UpdateWindow(feed, {icon: "/path/to/icon.svg"})
```

### Changing the show behavior
By default, an `UpdateWindow` is shown automatically when a new update is
available. You can change this with the `showOn` option:
```javascript
//Default behavior: Show when new update is available
new UpdateWindow(feed, {showOn: "will-quit"})

//Show when will-quit event is fired by Electron app
new UpdateWindow(feed, {showOn: "will-quit"})

//Only show when show() is called manually.
new UpdateWindow(feed, {showOn: "manual"})
```